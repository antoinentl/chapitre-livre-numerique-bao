= Formes, sources et supports du livre numérique
Antoine Fauchié, v1.0
:bibliography-database: bibliographie.bib
:bibliography-style: iso690-author-date-fr-no-abstract

////
bundle exec asciidoctor -b docbook --doctype article -r asciidoctor-bibliography bao-littnum-chapitre-2-formes-sources-et-supports-du-livre-numerique.adoc

pandoc --from docbook --to docx -o bao-littnum-chapitre-2-formes-sources-et-supports-du-livre-numerique.docx bao-littnum-chapitre-2-formes-sources-et-supports-du-livre-numerique.xml
////

//== Introduction
Le livre numérique fait partie des marronniers journalistiques{nbsp}: régulièrement, depuis une dizaine d'années, les organes de presse les plus variés se sentent dans l'obligation de publier un papier sur ce sujet.
Qu'y apprend-on{nbsp}?
Les huit premières années : que le livre numérique va venir remplacer le livre imprimé dans une perspective inéluctable.
Plus récemment : que la prophétie ne s'est pas réalisée, que tous le monde – et surtout les journalistes un peu déçus – s'est fait tromper.

Si le livre numérique est une thématique médiatique et académique récurrente, nous ne disposons pourtant pas d'une définition clairement établie.
De quoi parle-t-on lorsque nous évoquons le terme "livre numérique"{nbsp}?
La raison de cette quasi-absence est le spectre de l'_ebook_{nbsp}: il concerne autant le corps que les finances, l'industrie que l'artisanat, la typographie que les formats informatiques.
Chaque tentative de circonscrire cet objet à quelques notions facilement digérables se heurte à la diversité des métiers, des pratiques et des enjeux que touche le livre numérique.

Avant de tenter une nouvelle fois d'établir une délimitation, posons-nous la question suivante{nbsp}: où est le numérique dans le livre numérique{nbsp}?
Lorsque nous questionnons ce _nouveau_ mode de diffusion et d'accès au texte, nous abordons des thématiques telles que la numérisation, la dématérialisation puis l'enrichissement.
Le livre numérique serait numérique en cela qu'il se déchargerait de toute contrainte physique.
Les serveurs qui permettent à un lecteur d'accéder à un fichier EPUB sont pourtant bien matériels{nbsp}: il s'agit de bâtiments, d'ordinateurs, de câbles, avec des humains qui orchestrent tout cela.
Le qualificatif _numérique_ regroupe plutôt nos pratiques liées au Web{nbsp}: une accélération des processus de diffusion, une apparente démocratisation de l'accès à l'information, une dimension de flux dans la circulation des contenus et dans leur forme, un glissement du spectateur en acteur potentiel, et une reconfiguration des intervenants de la publication et de l'édition.

En plus d'inscrire une nouvelle définition du livre numérique, nous souhaitons découvrir ses diverses formes ainsi qu'embrasser les enjeux de diffusion liés aux pratiques d'écriture et de lecture.
Le numérique impose une approche en évolution constante, un _work in progress_.
Ce texte n'est donc qu'un humble instantané, une voix de plus dans une discussion globale.


== 1. Définitions du livre numérique

=== 1.1. Du premier livre numérique à l'écosystème de l'ebook
1971.
Il s'agit de la date du premier livre numérique.
Et plus précisément d'un simple fichier texte contenant la déclaration d'indépendance des États-Unis, une version numérisée puis diffusée par réseaux informatiques dans un cercle relativement restreint – à l'heure où Internet en était encore à ses prémices.
Michael Hart, alors étudiant à l'Université de l'Illinois, décide de partager cette version _dématérialisée_ du texte fondateur de l'Amérique du nord.
Ce sera également le point de départ d'un projet plus ambitieux{nbsp}: Gutenberg Projectfootnote:[Voir www.gutenberg.org.], la numérisation et la diffusion d'ouvrages issus du domaine public.
Il faudra quelques décennies pour que le livre numérique devienne une réalité commerciale, au-delà de ce projet trop confidentiel.
Pendant les années 2006-2007 trois événements vont permettre l'émergence de cette nouvelle forme de lecture, en terme d'expérience et de modèle économique, et ceci après plusieurs essais infructueux – oublions les prototypes de tablettes à affichage rétroéclairé de 10 centimètres d'épaisseur et de quelques kilos avec une autonomie ne dépassant pas l'heure.

Le premier événement est la mise en place d'un standard pour le format du livre numérique{nbsp}: quelques fichiers contenant des contenus structurés, un manifeste pour organiser ces chapitres ou sections, et une feuille de style pour agencer le texte.
Voilà de quoi est composé le format EPUB – ou ePub pour _electronic publication_.
Pour être plus concret il s'agit d'un site web _encapsulé_, portable.
Lisible sur tous les dispositifs ou presque, maintenu et documenté par l'International Digital Publishing Forum désormais intégré au World Wide Web Consortium, ce format est interopérable.
C'est un standard.
Le deuxième événement est la commercialisation d'un dispositif permettant une véritable _position_ de lecture.
Accessible financièrement, la liseuse à encre électronique a été et est encore le moyen le plus plébiscité pour _lire_ en numérique.
Ses atouts sont nombreux{nbsp}: confort de lecture, espace de stockage, durée de vie, personnalisation d'affichage, appareil monotâche.
Enfin, un catalogue de titres contemporains est venu enrichir des livres numériques provenant jusqu'ici majoritairement du domaine public.
Le livre _papier_ avait maintenant un concurrent.

Aujourd'hui nous nous sommes peut-être éloignés du projet initial et probablement utopique de Michael Hart, le livre numérique représente désormais plus un marché économique en éternel devenir qu'une démarche d'accès ouvert.
Les différents secteurs de la _chaîne du livre_ ont intégré ce compagnon du livre traditionnel – si tant est qu'il y est une figure immuable du livre physique, entre édition grand format, livre de poche et impression à la demande – comme une version incontournable.
Une grande majorité des éditeurs font du livre numérique sans pour autant que cela ne représente une manne financière considérable – les chiffres restent en dessous des 10% cite:[noauthor_les_2018] –, mais les pratiques évoluent.
Finalement nous sommes bien obligés de constater que l'écosystème de l'_ebook_ a été une reproduction du marché du livre avec, malgré tout, l'apparition de nouveaux acteurs.

==== 1.2. L'homothétie à l'œuvre
Définir le livre numérique, passé les aspects techniques et historiques présentés ci-dessus, consiste aussi à catégoriser ses différentes formes.
Rapidement, il est devenu nécessaire de distinguer l'_ebook_ empruntant les mêmes qualités et fonctionnalités que son homologue papier, du livre numérique comportant des enrichissements impossibles à matérialiser sur des pages reliées.
Le terme de "livre numérique homothétique" correspond donc à cet objet mimant son prédécesseur nommé "codex".
Ses caractéristiques sont – presque – les mêmes{nbsp}: suite de pages et navigation entre elles{nbsp}; organisation en chapitres qui peut se traduire par un sommaire{nbsp}; marque-page et surlignement{nbsp}; informations sur l'épaisseur.
En face, le livre numérique _enrichi_ est censé dépasser tout cela.
Plusieurs degrés peuvent être différenciés, allant du lien entre des pages – souvenez-vous des livres dont vous êtes le héros des années 1980 –, jusqu'à l'introduction de la réalité augmentée, en passant par du multimédia ou des processus de navigation plus complexes.
Ces différents niveaux d'enrichissement conduisent aussi parfois à remettre en cause le caractère homothétique, puisque pouvoir changer la typographie du texte est déjà une plus-value que ne propose pas un ouvrage imprimé.

Le marché du livre numérique s'est construit sur la version _homothétique_ de l'_ebook_.
Nous souhaitons ici proposer une hypothèse{nbsp}: si nous parlons d'homothétie pour la forme du livre, il conviendrait plutôt d'invoquer cet apanage pour son écosystème.
Jusqu'ici nous pourrions résumer à gros traits le marché du livre numérique à une reproduction du marché du livre imprimé, introduisant tout de même des acteurs inattendus et leurs nouvelles pratiques.
En effet si les premières années d'expérimentation et de défrichage ont permis de voir apparaître des structures indépendantes et innovantesfootnote:[Certaines d'entre elles ont progressivement fermé leur porte, comme l'éditeur Walrus à l'été 2018.], à l'opposé des grands groupes d'édition, la chaîne du livre a su reconstruire un écosystème très similaire à celui du livre imprimé.
Même si cela a pris du temps.
Les raisons sont sûrement économiques et légales, constituant certaines des limites permettant de définir le _livre numérique_.

=== 1.3. Limites dans le concept de livre numérique
Plutôt que d'énumérer les propriétés supposées du livre numérique, nous pouvons lister ses limites afin de délimiter un cadre.
La première limite est physique et concerne le corps, c'est l'un des points essentiels de _Après le livre_ de François Bon cite:[bon_apres_2011].
Lire nécessite une position, ou plutôt _des_ positions.
Cette question peut se résumer assez facilement par un cas concret{nbsp}: pouvez-vous prendre n'importe quelle posture pour lire avec le dispositif que vous utilisez{nbsp}?
Emportez votre ordinateur portable dans votre lit pour comprendre ce que vos positions de lecture seront vite limitées.
La liseuse à encre électronique permet de véritablement lire, tout comme un téléphone portable à grand écran – ou _smartphone_.

La seconde limite est le caractère clôt du livre{nbsp}: puisqu'il est très facile d'ajouter des pages et des sections à un site web, celui-ci n'est-il pas trop mouvant pour pouvoir être un livre{nbsp}?
Limite qui s'approche de celle liée à l'architecture des contenus{nbsp}: un article d'un carnet en ligne peut-il être considéré comme un livre{nbsp}?
La troisième limite est double{nbsp}: légale et économique.
Il s'agissait de l'un des principaux enjeux de la loi sur le prix unique du livre numérique{nbsp}: comment permettre à un marché d'éclore avec un objet bien identifiable tout en ne renfermant pas trop une définition trop consensuelle, peu enclin à favoriser les nouvelles pratiques et les nouveaux usages{nbsp}?
"Quel taux de TVA pour l'_ebook_{nbsp}?" a mis en lumière la différence entre le bien et le service{nbsp}: un fichier informatique est-il un service{nbsp}?
Un livre numérique dont seul l'accès est possible est-il un bien{nbsp}?


//=== 1.4. Conclusion
La définition du livre numérique est plurielle, mais elle est également mouvante.
Le récent débat sur l'accès à la lecture sous forme de _streaming_ – c'est-à-dire de flux, sans pouvoir _posséder_ le fichier – est révélateur cite:[benhamou_bien_2012].
Si nous pouvons dater l'arrivée du livre numérique aux années 2006 et 2007 avec les événements précédemment mentionnés, alors il est assez étonnant de constater qu'une définition légale n'est arrivée qu'en 2012 au Journal officielfootnote:[Voir la définition du livre numérique publiée dans le Journal officiel du 4 avril 2012. Ainsi le livre numérique a, indirectement, une définition fiscale qui suscité des débats entre la France et l'Europe{nbsp}: est-il un bien ou un service{nbsp}?], à la suite de la loi PULNfootnote:[Loi sur le prix unique du livre numérique, venant compléter la loi sur le prix unique du livre de 1981, dite loi Lang.].
Analysons désormais les formes du livre pour observer comment les usages – autant des auteurs et des éditeurs que des lecteurs – sont en évolution permanente.


== 2. Les formes numériques du livres

=== 2.1. L'EPUB au centre du marché du livre numérique
Le format EPUB constitue une avancée technique déterminante, ouvrant un marché jusqu'ici inexistant.
Grâce à la standardisation et à l'interopérabilité, les différents acteurs ont pu s'accorder pour _vendre_ des livres numériques.
L'avantage de cette conversion numérique est également ce qui freine le plus son essor{nbsp}: étant copiable à l'infini sans effort particulier, les éditeurs, distributeurs et libraires se voient dans l'obligation de protéger ce fichier avec un verrou.
La mesure technique de protection – en anglais DRM pour Digital Rights Management – d'Adobe est devenue l'un des premiers obstacles de tout lecteur numérique cite:[boulard_les_2016].
Aujourd'hui d'autres solutions existent – le tatouage ou la DRM open source LCP cite:[le_meur_principles_2016] –, censées faciliter l'expérience d'acquisition et d'ouverture du livre.

L'EPUB est portable, il renferme de nombreux fichiers qui s'apparentent à ceux d'un site webfootnote:[Des fichiers XHTML pour structurer le contenu, et des fichiers CSS pour mettre en forme cette structuration.], sa finalité est de reproduire l'expérience du livre imprimé{nbsp}: créer une spatialité similaire avec la pagination en premier lieu, lui conférant également une existence dans un circuit commercial.
Cette expérience est donc à la fois celle du lecteur, en achetant et en lisant un livre, mais aussi celle du libraire ou du revendeur, en disposant d'un objet reconnaissable et commercialisable.

Nous parlons ici de livres numériques homothétiques, identifiables par leur format – l'EPUB – et leur définition légale.
D'autres objets numériques que nous présentons par la suite peuvent être considérés comme des livres, mais cette reconnaissance ne leur est pour le moment pas ou peu accordée.
Découvrons tout d'abord les modalités d'accès à cet objet numérique bien identifié{nbsp}: le livre numérique au format EPUB.

=== 2.2. Accéder au livre numérique
Il existe plusieurs sources mettant à disposition des livres numériques au format EPUB, proposant des modes d'acquisition gratuits ou payants.
Il est important de noter que ces sites web, ces librairies en ligne ou ces plates-formes conditionnent la perception que nous pouvons nous faire des livres.

Une initiative réunit les nombreux sites web proposant des textes du domaine public, à vocation patrimoniale ou pour rendre disponible des classiques de la littérature.
Rassemblés sous le nom très éloquent de "Nos livres"footnote:[http://noslivres.net/], Gallica et Wikisource croisent éfélé ou le Projet Gutenberg.
La question du domaine public n'est pas non plus si simple en terme d'accès, puisque des sites français côtoient des plates-formes québécoises dont les droits ne sont pas similaires.

De l'autre côté, pourrions-nous dire, les sites de vente sont nombreux, considérés génériquement comme des librairies numériques, même s'il est difficile de mettre sur le même plan leslibraires.fr et Amazon.
Voici quelques exemples{nbsp}: la librairie _pure player_ numérique feedbooks{nbsp}; ePagine, une librairie numérique filiale de Tite-Live{nbsp}; Immatériel, d'abord distributeur et libraire, désormais l'un des principaux distributeurs indépendants français et créateur de la librairie 7switch{nbsp}; les incontournables iBookstore, Amazon, Kobo et Google Play Livres, les quatre premiers _vendeurs_ de livre numérique, des puissants mastodons financiers.

=== 2.3. D'autres formes du livre numérique
Le livre numérique, en terme d'inscription dans un marche économique mais aussi en terme d'usages, a une forme majoritairement homothétique.
Les plates-formes ou les points de vente sont nombreux et divers, mais néanmoins très bien identifiés.
Le format EPUB, le standard adopté par la grande majorité des acteurs du domaine, est le moyen de _vendre_ un livre en version numérique.
Pourtant, d'autres _formes_ existent, en marge des pratiques de lecture et des espaces marchands classiques.

L'une d'elles est le _livre web_, qui peut rapidement se définir comme un livre sous forme de site web cite:[fauchie_livre_2016].
Quelques points permettent d'identifier ce type de publication numérique{nbsp}: un ensemble de pages structurées dont l'accès est permis par un sommaire{nbsp}; des contenus qui s’adaptent à la taille de l’écran avec une interface de type _responsive web design_{nbsp}; des liens hypertextes qui permettent de naviguer dans et en-dehors du livre.
Certaines avancées techniques récentes permettent même de le lire sans connexionfootnote:[Une fois qu'une page a été affichée une fois dans le navigateur, il est possible de consulter l'ensemble des contenus sans être connecté à Internet.].
Plus besoin de télécharger un fichier comme l'EPUB, seule l'adresse et un navigateur web suffisent à lire en numérique.
Cette solution présente plusieurs limites, notamment la difficulté d'inscrire ce type d'ouvrage dans un circuit marchand – plus pour des raisons d'usage que techniques.

Cette forme du livre numérique est souvent le résultat d'initiatives individuelles, hors des circuits d'édition classiques, au sein de communautés spécifiques.
Certaines structures d'édition se sont accaparées ce modèle d'accès, comme OpenEdition{nbsp}: des presses universitaires qui privilégient cette forme comme l'un des accès les plus ouvert, proposant par ailleurs d'autres formats téléchargeables sous des conditions plus restrictives.
D'autres initiatives confidentielles existent, comme le mouvement de la littérature numérique offrant des prouesses de scénarisation en ligne, malheureusement trop méconnues et réservées à une communauté d'auteurs expérimentant véritablement avec le numérique.

//=== 2.4. Conclusion
La version homothétique et le format EPUB constituent sans aucun doute la forme la plus répandue, jusqu'ici, du livre numérique.
Il ne faut pour autant pas ignorer des modes de lecture plus confidentiels, et souvent plus proches des usages du Web.
Nous pouvons envisager une réelle convergence entre les facilités d'accès et d'usage permises par le Web d'une part, et l'écosystème que constitue un riche catalogue commercialisé et des acteurs spécialisés de l'autre.
Si la définition du livre numérique est plurielle, c'est également le cas de sa forme.
Pour dépasser ces questionnements nous devons désormais nous intéresser à une autre dimension de l'_ebook_{nbsp}: les nouveaux modes d'écriture et de diffusion.

== 3. Écrire, publier, diffuser

=== 3.1. Literacy numérique
L'_ebook_ porte avec lui la promesse d'une _literacy_ numérique{nbsp}: la constitution d'une culture, la compréhension d'un environnement et la maîtrise d'outils spécifiques cite:[dacos_read-write_2009].
À la fois en tant que _lecteur_ pour trouver, consulter et s'approprier des textes{nbsp}; et en tant qu'_écrivant_ ou _auteur_ pour produire et diffuser des œuvres.

Si le livre imprimé nécessite des compétences pour la réalisation du fichier envoyé à l'imprimeur, et d'une infrastructure pour la production de l'objet, le livre numérique requiert moins d'effort pour être finalisé et transmis.
Le fichier EPUB peut être fabriqué relativement facilement, à l'aide de logiciels spécialisés accessibles et gratuits pour certainsfootnote:[Le logiciel Sigil (https://sigil-ebook.com/) est un exemple emblématique.].
Sa diffusion, même si elle sera limitée, peut être réalisée via un simple site web, avec l'aide de réseaux sociaux ou de plates-formes dédiées.
Depuis plusieurs années des services d'aide à l'auto-édition sont apparus, certains payants, sans parler des blogs ou des carnets en ligne.

Ces moyens, désormais disponibles pour des pratiques que nous pourrions qualifier d'_amateur_, sont bien moins puissants que les outils professionnels développés par les maisons d'édition et les diffuseurs/distributeurs, mais ils existent.
Le potentiel ainsi offert ne doit pas être sous-estimé, pas plus qu'il ne faut croire que ces outils de _publication_ feront automatiquement de tout auteur son propre éditeur.

=== 3.2. Visibilité sur le Web
Fabriquer un objet numérique et le rendre disponible n'est pas le plus difficile, même si un apprentissage est nécessaire.
La visibilité sur le Web est une question bien plus délicate, et plus globalement la constitution d'une communauté qui demande de gérer des aspects communicationnels complexes et particulièrement chronophages.
Les premiers réseaux de carnets en ligne furent l'occasion pour une poignée d'auteurs pionniers de créer des espaces de diffusion alternatifs et peut-être trop discrets.

Aujourd'hui un certain nombre de plates-formes tendent à remplacer ce maillage dense et indépendant, Wattpad étant l'une des plus visibles, masquant la diversité permise par les sites web personnels ou collectifs.
La logique des silosfootnote:[Le concept de "silo" fait référence au caractères hermétique des plates-formes comme certains réseaux sociaux.] questionne sur la dimension de centralisation et sur l'abandon d'une maîtrise par les auteurs et d'un enfermement pour les lecteurs.

=== 3.3. Le livre numérique{nbsp}: une opportunité
Le livre numérique est une occasion de mettre à la portée des auteurs des outils de création et de diffusion, et d'offrir aux lecteurs des espaces de découverte non conventionnels.
Le Web remplace la photocopieuse de l'ère du fanzine – pour comparer ce phénomène à celui de la culture underground.

Il serait dommage de laisser à quelques acteurs hégémoniques le monopole de la création et de la diffusion de la littérature ou plus globalement des textes.
Les premiers gestes d'écriture numérique ont accompagnés le mouvement d'Internet, laissant une liberté importante pour les créateurs.
Il ne faut pas que cet espace de création se referme et que les auteurs perdent la maîtrise qu'ils ont su construire et transmettre.
La seule logique marchande ne doit pas diriger la circulation d'une littérature empreinte d'expérimentation et de diversité.
Il est encore temps de transformer un modèle qui s'impose à nous.

== Conclusion
L'initiative originelle du livre numérique est peut-être encore intacte{nbsp}: donner accès à des textes avec une nouvelle ambition.
Et les bibliothèques sont sûrement les mieux placées pour perpétuer cette volonté de diffusion et d'acculturation.
Le livre numérique en tant que marché est certes encore restreint à sa dimension homothétique, laissant peu de place aux expérimentations littéraires et numériques.
Il nous faut cultiver des espaces non marchands autant pour irriguer des canaux économiques que pour construire et alimenter une culture dont nous sommes les acteurs.

Les formes du livre non conventionnelles comme le _livre web_ sont des opportunités de composer de nouvelles pratiques de lecture, empreintes du Web et de _literacy_.
Il nous faut, et d'autant plus en tant que professionnels du livre, participer à ce double mouvement de création et de diffusion, prendre part au flux sans le laisser nous submerger.

== Bibliographie
Bibliographie en ligne sur Zotero : +
https://www.zotero.org/antoinentl/items/collectionKey/Y7227YIJ

bibliography::[]
