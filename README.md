Ceci est le dépôt des textes pour la rédaction d'un chapitre sur le livre numérique (au sens large) dans un ouvrage collectif.

# Résumé du chapitre
Le livre numérique nécessite d'être défini, non pas pour l'enfermer dans un cadre précis, mais bien plutôt pour appréhender toutes ses spécificités et comprendre les formes diverses qu'il incarne actuellement et qu'il pourra incarner.
Du premier livre numérique dans les années 1970 au trop confidentiel "livre web", il s'agit d'accepter que la signification du livre est plurielle.
Les questions techniques – notamment le format EPUB, standard du livre numérique – doivent être présentées avec une approche globale : qu'apporte ce format ?
Quelles opportunités représente-t-il ?
Enfin, évoquer le livre numérique ne peut faire l'économie de l'écriture numérique : il s'agit autant de diffuser que d'inscrire, et ainsi de se saisir de la dimension de flux de cet objet numérique.

# Plan du chapitre

## Introduction
De quoi parle-t-on ?
Le numérique impose une approche _work in progress_ : les choses bougent, et elles bougent vite.
Annonce du plan : définir le livre numérique, découvrir ses formes, comprendre les enjeux de diffusion.


## 1. Définitions du livre numérique
Du premier livre numérique (1971, Michael Harts) à des formes hybrides en passant par le très populaire livre numérique homothétique et le trop confidentiel "livre web".

Deux points importants : la dimension de "limite" dans le concept de livre (un billet de blog n'est pas un livre numérique), et les contraintes légales (mouvantes).

Il s'agit d'accepter que la définition du livre numérique est plurielle.

## 2. Les formes numériques du livres
Définition du format EPUB (site web encapsulé) pour revenir au web :

- questions autour de la _portabilité_ des objets numériques (point lecture hors connexion) et du partage (plus facile d'échanger une adresse qu'un fichier) ;
- carnets en ligne, concept de livre web.

Exemples concrets de sources de livres numériques :

- les bibliothèques numériques (dont patrimoniales) et leurs limites (notamment Wikisource) : noslivres.net, Gallica, Wikisource, etc.
- les librairies numériques : l'économie numérique ;
- la question des satellites numériques : sites web indépendants, licences ouvertes, etc.

## 3. Écrire, publier, diffuser
Le livre numérique porte avec lui la promesse d'une _literacy_ numérique : lire, écrire. Les plates-formes numériques en sont la preuve. Exemples de WordPress et de Wattpad.

De la mise à disposition à la publication : la délicate question de la visibilité sur le Web.

S'abonner : d'une logique marchande (Amazon) à l'ouverture (flux RSS des sites, carnets et blogs).

## Conclusion
Et la boucle est bouclée : recréer du flux à partir du flux. Les bibliothèques participent de ce foisonnement.
